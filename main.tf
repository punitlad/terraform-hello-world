provider "aws" {
  profile    = "default"
  region     = "us-east-2"
}

terraform {
  backend "s3" {
    bucket = "states-terraform"
    key    = "terraform-hello-world/terraform.tfstate"
    region = "us-east-2"
  }
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-trusty-14.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_instance" "web" {
  ami           = "${data.aws_ami.ubuntu.id}"
  instance_type = "t2.micro"
}

resource "aws_s3_bucket" "hello_world_bucket" {
  bucket = "hello-world-bucket"
  acl    = "private"
}